//
//  SocketIO.swift
//  WhereZZApp
//
//  Created by Admin on 01/02/21.
//

import Foundation
import SocketIO


class SocketRouter : NSObject{
    
    //MARK: - VARIABLES -
    static let shared = SocketRouter()
    
    let manager = SocketManager(socketURL: URL(string: AppConstants.socketEventURL)!, config: [.log(true), .compress])
    var socket: SocketIOClient!
    
    //MARK: - METHODS -
    func connect() {
        guard socket != nil, socket.status == .connected else {
            socket = manager.defaultSocket
            self.listenEvent()
            socket.connect()
            return
        }
    }
    
    func disconnect() {
        if socket != nil {
            socket.disconnect()
        }
    }
    
    func joinRoom(sender_id:String) {
        guard socket.status == .connected else {
            print("Not connected")
            return
        }
        socket.emit(socketEvents.login, sender_id) {
            print("connected to room \(sender_id)")
        }
    }
    
    //MARK: - GET ALL MESSAGES -
    func getMessagesList(user_id:Int,contact_id:Int, reshandler: @escaping (_ res: [LastMessageModel]?) -> Void) {
        socket.emitWithAck(socketEvents.getMessagesList,user_id,contact_id).timingOut(after: 5.0) { (res) in
            if let res = res[0] as? NSArray {
                do {
                    let jsonData = try JSONSerialization.data(withJSONObject: res, options: .prettyPrinted)
                    let decoder = JSONDecoder()
                    decoder.keyDecodingStrategy = .convertFromSnakeCase
                    let _response = try decoder.decode([LastMessageModel].self, from: jsonData)
                    reshandler(_response)
                } catch let parsingError {
                    print("Error", parsingError.localizedDescription)
                    reshandler(nil)
                    showMessage(text: Messages.somethingwentwrong)//Messages.somethingwentwrong)
                }
            }else{
                reshandler(nil)
            }
        }
    }
    
    //MARK: - GET ALL USERS LIST -
    func getUsersList(user_id:Int, reshandler: @escaping (_ resModel: [UserChatListResponse]?) -> Void) {
        socket.emitWithAck(socketEvents.getUsersList, with: [user_id]).timingOut(after: 5.0) { (res) in
            if let res = res[0] as? NSArray {
                do {
                    let jsonData = try JSONSerialization.data(withJSONObject: res, options: .prettyPrinted)
                    let decoder = JSONDecoder()
                    decoder.keyDecodingStrategy = .convertFromSnakeCase
                    let _response = try decoder.decode([UserChatListResponse].self, from: jsonData)
                    reshandler(_response)
                } catch let parsingError {
                    print("Error", parsingError.localizedDescription)
                    reshandler(nil)
                    showMessage(text: Messages.somethingwentwrong)//Messages.somethingwentwrong)
                }
            }else{
                reshandler(nil)
            }
        }
    }
    
    //MARK: - SEND MESSAGE -
    func sendMessage(_ obj : [String:Any]) {
        guard socket.status == .connected else {
            print("Not connected")
            return
        }
        socket.emit(socketEvents.sendMessage,obj) {
            print("Sent Message : ",obj)
        }
    }
    
    
    //MARK: - SET OBSERVER -
    func setGetUsersObserver(reshandler: @escaping (_ rerModel: LastMessageModel?) -> Void) {
        socket.on(socketEvents.getMsgObserver){ (res, ack) -> Void in
            if let res = res[0] as? NSArray {
                do {
                    let jsonData = try JSONSerialization.data(withJSONObject: res, options: .prettyPrinted)
                    let decoder = JSONDecoder()
                    decoder.keyDecodingStrategy = .convertFromSnakeCase
                    let _response = try decoder.decode(LastMessageModel.self, from: jsonData)
                    reshandler(_response)
                } catch let parsingError {
                    print("Error", parsingError.localizedDescription)
                    reshandler(nil)
                }
            }else{
                reshandler(nil)
            }
        }
    }
    
    func removeGetObserver(){
        socket.off(socketEvents.getMsgObserver)
    }

    func listenEvent() {
        socket.on(clientEvent: .connect) {data, ack in
            print("socket connected from listen event")
            if isChatNotification {
                if Utilities.shared.socketConnected != nil {
                    Utilities.shared.socketConnected!()
                }
            }
        }
        socket.on(clientEvent: .disconnect) {data, ack in
            print("socket disconnected from listen event")
        }
        socket.on(socketEvents.login) { (data, ack) -> Void in
            print(data,ack)
        }
        socket.on(socketEvents.getMessagesList) { (data, ack) -> Void in
            print(data,ack)
        }
        socket.onAny { (any) in
            print(any)
        }
    }
    
    func onMessage(reshandler: @escaping (_ res: Any) -> Void) {
        socket.on("get_message"){ (data, ack) -> Void in
            reshandler(data)
        }
    }
}
